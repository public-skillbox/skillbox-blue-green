# Task

Blue-green site deployment with rollback
Configure the deployment so that the title of the staging site displays the name of the branch from which the deployment was made, in the format [branch=$branch].

