#!/bin/bash

if [[ -d "$docker_html_path/test-app/$CI_COMMIT_SHA/prev-version" ]]; then
  cp -Pv --remove-destination $docker_html_path/test-app/$CI_COMMIT_SHA/prev-version $docker_html_path/html
else 
  echo "Directory $docker_html_path/test-app/$CI_COMMIT_SHA/prev-version does not exist"
fi

echo "docker_html_path: $docker_html_path"
echo "CI_COMMIT_SHA: $CI_COMMIT_SHA"
cp -Pv --remove-destination $docker_html_path/test-app/$CI_COMMIT_SHA/prev-version $docker_html_path/html
